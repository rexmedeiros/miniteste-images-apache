FROM httpd:2.4.46-alpine

RUN apk update; \
    apk upgrade;

# Include apache vhost file to proxy php requests to php-fpm container
RUN echo "Include /usr/local/apache2/conf/conf-extra/httpd-extra.conf" \
    >> /usr/local/apache2/conf/httpd.conf

EXPOSE 80 8080-8090
